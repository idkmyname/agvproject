
#include <AccelStepper.h> //steppenmotor library

//stepper motor pins
#define dirPinL 7
#define stepPinL 8
#define dirPinR 9
#define stepPinR 4

AccelStepper stepperL(1, stepPinL, dirPinL);
AccelStepper stepperR(1, stepPinR, dirPinR);
//motor interface type must be set to 1 when using a driver.

//stopknop
#define stopknopPin x

//ultrasoonsensoren
//yeet
#define echoPinL 12
#define trigPinL 11
#define echoPinM 5
#define trigPinM 6
#define echoPinR 3
#define trigPinR 2

//infraroodsensoren randen dichtbij
#define irRandenDichtbijPinL A2
#define irRandenDichtbijPinR A1

//infraroodsensoren randen ver
#define irRandenVerPinL A3
#define irRandenVerPinR A0

//infraroodsensoren bomen
#define irBomenPinL A4
#define irBomenPinR A5

//knoppen
#define VolgknopDetect 0
#define StopknopDetect 1
int Stopknop = 1;
int Volgknop = 0;

/*==============================================================*/
//Declaraties variabelen
/*==============================================================*/

//schakelen tussen modi
int modus = 1;

//bochten maken
int links = 0;
int rechts = 0;

//ultrasoonsensoren
long duration, distance, RightSensor, FrontSensor, LeftSensor;

//infraroodsensoren dichtbij
int irRandenDichtbijL = 1;
int irRandenDichtbijR = 1;

//infraroodsensoren ver
int irRandenVerL = 0;
int irRandenVerR = 0;

//infraroodsensoren bomen
int irBomenL = 1;
int irBomenR = 1;


int x = 0;
int y = 0;
int z = 0;
int a = 0;
int b = 800;
int c = 800;
int d = 0;
int m = 0;
int bochtafteller = 0;
int bochtaantal = 1;

/*==============================================================*/
//Configuraties
/*==============================================================*/

void setup()
{
  Serial.begin(9600);
  pinMode(trigPinL, OUTPUT);
  pinMode(echoPinL, INPUT);
  pinMode(trigPinR, OUTPUT);
  pinMode(echoPinR, INPUT);
  pinMode(trigPinM, OUTPUT);
  pinMode(echoPinM, INPUT);
  pinMode(VolgknopDetect, INPUT_PULLUP);
  pinMode(StopknopDetect, INPUT_PULLUP);
  stepperL.setMaxSpeed(1000); //snelheid (max stappen per seconden)
  stepperR.setMaxSpeed(1000);
  stepperL.setAcceleration(60);
  stepperR.setAcceleration(60);
  bepaalstart();
}


/*==============================================================*/
//Code
/*==============================================================*/

void loop()
{
  if (a == 80) {
    ultrasoon();
    a = 0;
  }
  a = a + 1;
  if (bochtafteller > 0) {
    bochtafteller = bochtafteller - 2;
  }
  infrarood();
  knoppen();


  if (Stopknop == 0) //misschien aanpassen
  {
    while (Stopknop == 0)
    {
      stepperL.setSpeed(0);
      stepperR.setSpeed(0);
      Stopknop = digitalRead(StopknopDetect);
    }
  }

  if (modus == 1 || modus == 3) {
    if (LeftSensor <= 7 || FrontSensor <= 8 || RightSensor <= 7) { //misschien de grenswaarden aanpassen
      if ((irRandenVerL == 0) && (irRandenVerR == 0)) {
        mensDetecteren();
      }
    }
    else {
      modus = 1;
    }
  }
/*
  if (modus == 3 || modus == 4) {
    if (Volgknop == 0) {
      volgen();
    }
  }
*/
  if (y == 0) {
    if (modus == 1) {
      if (bochtaantal < 3) {
        if (irBomenR == 0) {
          if (rechts == 1) {
            boomDetecteren();
          }
        }
        if (irBomenL == 0) {
          if (links == 1) {
            boomDetecteren();
          }
        }
      }
    }
  }
  if ( y > 0) {
    y = y - 1;
  }
  if ((bochtaantal == 1) || (bochtaantal == 2)) {
    if (modus == 1) {
      if (irRandenVerL == 1 && irRandenVerR == 1) {
        if (FrontSensor <= 7) { //misschien aanpassen
          if (links == 1 || rechts == 1) {
            if (bochtafteller == 0) {
              bochtMaken();
            }
          }
        }
      }
    }
  }
  if (bochtaantal == 0) {
    if (modus == 1) {
      if (irRandenVerL == 1 && irRandenVerR == 1) {
        if (FrontSensor <= 7) { //misschien aanpassen
          if (links == 1 || rechts == 1) {
            if (bochtafteller == 0) {
              bochtMaken(); //mischien een kleinere bocht voor deze
            }
          }
        }
      }
    }
  }

  if (modus == 1) {
    if (bochtaantal != 0) {
      normaal();
    }
  }

  if (modus == 1) {
    if (bochtaantal == 0) {
      if (links == 1) {
        klevenL();
      }
    }
  }
  
  if (modus == 1) {
    if (bochtaantal == 0) {
      if (rechts == 1) {
        klevenR();
      }
    }
  }
  if (bochtaantal == 3) {
    if (FrontSensor <= 7) {
      if (irRandenVerL == 1 || irRandenVerR == 1) {
        while (1) {
          //einde!
        }
      }
    }
  }


}


void bepaalstart()
{
  irRandenVerL = digitalRead(irRandenVerPinL);
  irRandenVerR = digitalRead(irRandenVerPinR);
  if (irRandenVerL == 0 && irRandenVerR == 1) {
    rechts = 1;
  }
  if (irRandenVerR == 0 && irRandenVerL == 1) {
    links = 1;
  }
  bochtafteller = 1000;
}

void knoppen()
{
  Volgknop = digitalRead(VolgknopDetect);
  Stopknop = digitalRead(StopknopDetect);
}

void ultrasoon()
{
  SonarSensor(trigPinR, echoPinR);
  RightSensor = distance;
  SonarSensor(trigPinL, echoPinL);
  LeftSensor = distance;
  SonarSensor(trigPinM, echoPinM);
  FrontSensor = distance;

  Serial.print(LeftSensor);
  Serial.print(" - ");
  Serial.print(FrontSensor);
  Serial.print(" - ");
  Serial.println(RightSensor);
}


void SonarSensor(int trigPin, int echoPin)
{
  digitalWrite(trigPin, LOW);
  stepperL.runSpeed();
  stepperR.runSpeed();
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  stepperL.runSpeed();
  stepperR.runSpeed();
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration / 2) / 29.1;
  stepperL.runSpeed();
  stepperR.runSpeed();
  delay(15); //mischien korter
  //misschien hier ook runspeed
}


void infrarood()
{
  irRandenDichtbijL = digitalRead(irRandenDichtbijPinL);
  irRandenDichtbijR = digitalRead(irRandenDichtbijPinR);

  irRandenVerL = digitalRead(irRandenVerPinL);
  irRandenVerR = digitalRead(irRandenVerPinR);

  irBomenL = digitalRead(irBomenPinL);
  irBomenR = digitalRead(irBomenPinR);
}

void klevenL()
{
  if (irRandenDichtbijL == 1 && irRandenDichtbijR == 1)
  {
    Serial.println("normaal");
    stepperL.setSpeed(900);
    stepperR.setSpeed(900);
    stepperL.runSpeed();
    stepperR.runSpeed();
    //step the motor with constant speed as set by setSpeed()
  }

  if ((irRandenDichtbijL == 0) && (m == 0)) //stuur naar rechts
  {
    Serial.println("normaal rechts");
    stepperL.setSpeed(500);
    stepperR.setSpeed(-100);
    stepperL.runSpeed();
    stepperR.runSpeed();
    z = 1;
  }
  m = 0;

  if ((irRandenVerPinL == 0) && (z == 0)) //stuur naar rechts
  {
    Serial.println("normaal links");
    stepperR.setSpeed(500);
    stepperL.setSpeed(-100);
    stepperL.runSpeed();
    stepperR.runSpeed();
    m = 1;
  }
  z = 0;
}

void klevenR()
{
  if (irRandenDichtbijL == 1 && irRandenDichtbijR == 1)
  {
    Serial.println("normaal");
    stepperL.setSpeed(900);
    stepperR.setSpeed(900);
    stepperL.runSpeed();
    stepperR.runSpeed();
    //step the motor with constant speed as set by setSpeed()
  }

  if ((irRandenDichtbijR == 0) && (m == 0)) //stuur naar links
  {
    Serial.println("normaal rechts");
    stepperL.setSpeed(-100);
    stepperR.setSpeed(500);
    stepperL.runSpeed();
    stepperR.runSpeed();
    z = 1;
  }
  m = 0;

  if ((irRandenVerPinR == 0) && (z == 0)) //stuur naar rechts
  {
    Serial.println("normaal links");
    stepperR.setSpeed(-100);
    stepperL.setSpeed(700);
    stepperL.runSpeed();
    stepperR.runSpeed();
    m = 1;
  }
  z = 0;
}

void normaal()
{


  //steps per second

  if (irRandenDichtbijL == 1 && irRandenDichtbijR == 1)
  {
    Serial.println("normaal");
    stepperL.setSpeed(900);
    stepperR.setSpeed(900);
    stepperL.runSpeed();
    stepperR.runSpeed();
    //step the motor with constant speed as set by setSpeed()
  }

  if ((irRandenDichtbijL == 0) && (m == 0)) //stuur naar rechts
  {
    Serial.println("normaal rechts");
    stepperL.setSpeed(-200);
    stepperR.setSpeed(700);
    stepperL.runSpeed();
    stepperR.runSpeed();
    z = 1;
  }
  m = 0;

  if ((irRandenDichtbijR == 0) && (z == 0)) //stuur naar links
  {
    Serial.println("normaal links");
    stepperR.setSpeed(-200);
    stepperL.setSpeed(700);
    stepperL.runSpeed();
    stepperR.runSpeed();
    m = 1;
  }
  z = 0;
}


void bochtMaken()
{
  Serial.println("bocht");
  //maak waarschuwingsgeluid

  if (links == 1)
  {
    x = 0;
    while (x < 1100) { //1 sec?
      stepperL.setSpeed(80); //steps per second
      stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
      stepperR.setSpeed(-80); //steps per second
      stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
      x = x + 1;
      Serial.println(x);
    }
    x = 0;
    while (x < 900) { //1 sec?
      stepperL.setSpeed(200); //steps per second
      stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
      stepperR.setSpeed(200); //steps per second
      stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
      x = x + 1;
      Serial.println(x);
    }
    x = 0;
    while (x < 1600) { //1 sec?
      stepperL.setSpeed(80); //steps per second
      stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
      stepperR.setSpeed(-80); //steps per second
      stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
      x = x + 1;
      Serial.println(x);
    }
    x = 0;
    ultrasoon();
    bochtafteller = 1000;
    bochtaantal = bochtaantal + 1;
    links = 0;
    rechts = 1;
    return;
  }

  if (rechts == 1)
  {
    x = 0;
    while (x < 1100) { //1 sec?
      stepperL.setSpeed(-80); //steps per second
      stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
      stepperR.setSpeed(80); //steps per second
      stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
      x = x + 1;
      Serial.println(x);
    }
    x = 0;
    while (x < 900) { //1 sec?
      stepperL.setSpeed(200); //steps per second
      stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
      stepperR.setSpeed(200); //steps per second
      stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
      x = x + 1;
      Serial.println(x);
    }
    x = 0;
    while (x < 1600) { //1 sec?
      stepperL.setSpeed(-80); //steps per second
      stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
      stepperR.setSpeed(80); //steps per second
      stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
      x = x + 1;
      Serial.println(x);
    }
    x = 0;
    ultrasoon();
    bochtafteller = 1000;
    bochtaantal = bochtaantal + 1;
    links = 1;
    rechts = 0;
    return;
  }
}


void boomDetecteren()
{
  x = 500;
  while (x > 0) {
    Serial.println("boom");
    stepperL.setSpeed(0);
    stepperR.setSpeed(0);
    x = x - 1;
    //maak geluid of licht
  }
  y = 1000;
}


void mensDetecteren()
{
  Serial.println("mens");
  stepperL.setSpeed(0);
  stepperR.setSpeed(0);
  modus = 3;
}


void volgen()
{
  modus = 4;

  if (FrontSensor >= 6)
  {
    if (irRandenDichtbijL == 1 && irRandenDichtbijR == 1) //AGV rijdt rechtuit als hij zich in het midden van het pad bevindt
    {
      stepperL.setSpeed(900);
      stepperR.setSpeed(900);
      stepperL.runSpeed();
      stepperR.runSpeed();
      Serial.println("volgen rechtuit");
      //step the motor with constant speed as set by setSpeed()
    }

    if (irRandenDichtbijL == 0) //AGV stuurt naar rechts als hij te dicht bij de linkerrand komt
    {
      stepperL.setSpeed(900);
      stepperR.setSpeed(-50);
      stepperL.runSpeed();
      stepperR.runSpeed();
      Serial.println("volgen rechts");
    }

    if (irRandenDichtbijR == 0) //AGV stuurt naar links als hij te dicht bij de rechterrand komt
    {
      stepperR.setSpeed(900);
      stepperL.setSpeed(-50);
      stepperL.runSpeed();
      stepperR.runSpeed();
      Serial.println("volgen links");
    }
  }
  else {
    stepperL.setSpeed(0);
    stepperR.setSpeed(0);
  }

  if (irRandenVerL == 1 || irRandenVerR == 1) //Voorwaarde: AGV staat voor een kruising, splitsing of bocht omdat er aan ten minste één kant geen rand is
  {
    if (RightSensor < 7 && irRandenVerR != 0) //AGV slaat rechtsaf als er aan de rechterkant geen hek is en de rechter ultrasoonsensor een hand detecteert
    {
      //maak een bocht van 180 graden naar rechts
      Serial.println("volgen bocht rechts");
    }

    if (LeftSensor < 7 && irRandenVerL != 0) //AGV slaat linksaf als er aan de linkerkant geen hek is en de linker ultrasoonsensor een hand detecteert
    {
      //maak een bocht van 90 graden naar links
      Serial.println("volgen bocht links");
    }

    else //In alle andere gevallen: AGV rijdt rechtuit
    {
      stepperL.runSpeed();
      stepperR.runSpeed();
      Serial.println("volgen bocht rechtuit");
      //step the motor with constant speed as set by setSpeed()
    }
  }


  if (Volgknop == 1)
  {
    modus = 1; //normaal
  }

}
