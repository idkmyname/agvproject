/*Example sketch to control a stepper motor with A4988 stepper motor driver AccelStepper library and Arduino. More info: https://www.makerguides.com */

#include <AccelStepper.h>

//Define stepper motor connections
#define dirPinL 7
#define stepPinL 8
#define dirPinR 9
#define stepPinR 4

//Create stepper object
AccelStepper stepperL(1, stepPinL, dirPinL);
AccelStepper stepperR(1, stepPinR, dirPinR);

//motor interface type must be set to 1 when using a driver.
int x = 0;

void setup()
{
  Serial.begin(9600);
  stepperL.setMaxSpeed(200);
  stepperR.setMaxSpeed(200);
  //maximum steps per second
  stepperL.setAcceleration(60);
  stepperR.setAcceleration(60);
}

void loop()
{
  while (x < 1700) { //1 sec?
    stepperL.setSpeed(-50); //steps per second
    stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
    stepperR.setSpeed(50); //steps per second
    stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
    x = x + 1;
    Serial.println(x);
  }
  x = 0;
    while (x < 700) { //1 sec?
    stepperL.setSpeed(200); //steps per second
    stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
    stepperR.setSpeed(200); //steps per second
    stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
    x = x + 1;
    Serial.println(x);
  }
  x = 0;
    while (x < 1700) { //1 sec?
    stepperL.setSpeed(-50); //steps per second
    stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
    stepperR.setSpeed(50); //steps per second
    stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
    x = x + 1;
    Serial.println(x);
  }
  x = 0;
  delay(3000);
}
