/*Example sketch to control a stepper motor with A4988 stepper motor driver AccelStepper library and Arduino. More info: https://www.makerguides.com */

#include <AccelStepper.h>

//Define stepper motor connections
#define dirPinL 7
#define stepPinL 8
#define dirPinR 9
#define stepPinR 4

//Create stepper object
AccelStepper stepperL(1, stepPinL, dirPinL);
AccelStepper stepperR(1, stepPinR, dirPinR);
//motor interface type must be set to 1 when using a driver.
int x = 0;
int y = 800;


void setup()
{
  Serial.begin(9600);
  stepperL.setMaxSpeed(1000);
  stepperR.setMaxSpeed(1000);
  stepperL.setAcceleration(60);
  stepperR.setAcceleration(60);
  //maximum steps per second
}

void loop()
{
  stepperL.setSpeed(600);
  stepperR.setSpeed(600);
  //bocht naar rechts
  stepperR.moveTo(y);
  y = y + 1250;
  stepperR.runToPosition();
  while (x < 500) { //1 sec?
    stepperL.setSpeed(200); //steps per second
    stepperL.runSpeed(); //step the motor with constant speed as set by setSpeed()
    stepperR.setSpeed(200); //steps per second
    stepperR.runSpeed(); //step the motor with constant speed as set by setSpeed()
    x = x + 1;
    Serial.println(x);
  }
  x = 0;
  stepperL.setSpeed(600);
  stepperR.setSpeed(600);
  stepperR.moveTo(y);
  y = y + 800;
  stepperR.runToPosition();
  delay(3000); //3 sec?
}
